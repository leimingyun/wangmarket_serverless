<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="参数设置"/>
</jsp:include>

<style>
	
	.content{
		width: 600px;
		min-height:80%;
   margin: 0 auto;
   box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
   padding: 30px;
   margin-top: 50px;
	}
	.title{
		border-bottom: 1px solid #eee;
   padding-top: 20px;
   padding-left: 10px;
   padding-bottom: 20px;
   font-size: 28px;
   margin-bottom: 20px;
   text-align:center;
	}
	.content ul{
		padding-left: 20px;
	}
	.content ul li{
		list-style-type: decimal;
		padding-left:10px;
		padding-bottom:4px;
	}
	.content ul li img{
		max-width:250px;
		padding:4px;
		padding-left:40px;
	}
	.info{
		font-size:14px;
		line-height: 22px;
	}
	.info h2,h3,h4,h5{
	border-bottom: 1px solid #eee;
   padding-top: 23px;
   margin-bottom: 10px;
   padding-bottom: 5px;
	}
	
	@media only screen and (max-width: 700px) {
		.content{
			width:auto;
			margin-top: 0px;
			box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
		}
		
	}
	
	a{
		color: blue;
	}
</style>

<div class="content">
	<div class="title">
		请选择您的域名是否已备案？
	</div>
	
	<div class="select">
		<div class="selectItem" onclick="setArea('cn-north-4');">
			<div class="title">已备案</div>
			<div class="intro">
				如果你的域名已备案、或要正常备案后使用，请选择此，将网站的文件存储在国内，访问时有更好的打开速度及极致体验。
			</div>
		</div>
		
		<div class="selectItem" id="weishouquanyun" onclick="setArea('ap-southeast-1');">
			<div class="title">未备案</div>
			<div class="intro">
				如果你不打算将域名备案，可选择此，采用香港节点来进行存储网站数据。而香港，未备案域名是可以正常访问的。<br/>
				现在网络的完善程度，虽然存储在香港，但在国内使用基本也感觉不出什么差别了，还是棒棒的。
			</div>
		</div>
		
	</div>
		
		
	
	<div class="info" style="font-size:16px; display:none;">
	
		<div style="float:left; width:50%;">endpoint:<%=SystemUtil.get("HUAWEIYUN_COMMON_ENDPOINT") %></div>
		<div style="float:left">
			点此设置：
			<select id="selectId" onchange="selectChange();" class="layui-btn layui-btn-primary">
				<option value="" >请选择服务区域</option>
				<option value="cn-north-4" >华北-北京四</option>
				<option value="cn-north-1" >华北-北京一</option>
				<option value="cn-east-2" >华东-上海二</option>
				<option value="cn-east-3" >华东-上海三</option>
				<option value="cn-south-1" >华南-广州</option>
				<option value="cn-southwest-2" >西南-贵阳一</option>
				<option value="ap-southeast-1" >亚太-香港</option>
				<option value="ap-southeast-3" >亚太-新加坡</option>
				<option value="ap-southeast-2" >亚太-曼谷</option>
			</select>
		</div>
	</div>
</div>



<style>
.select{
	padding:20px;
}
.select>div{
	padding: 30px;
    border: 1px;
    border-style: solid;
    border-color: #d2d2d236;
}
.select div .title{
	font-size: 20px;
}
.select div .intro{
	font-size: 14px;
}


.lilist{
	padding-left: 40px;
}
.lilist>li{
	list-style: decimal;
}

.selectItem{
	cursor: pointer;
}
.selectItem:HOVER{
	border-color: #000000;
	background-color: #FAFAFA;
}
</style>

<script type="text/javascript">
/**
 * 设置区域。
 * param area 设置区域，传入如 cn-north-4
 */
function setArea(area){
	msg.loading("设置中...此过程耗时较长，请耐心等待");	//显示“更改中”的等待提示
	post("/plugin/serverless/create.json", { "area": area}, function(data){
		msg.close();	//关闭“更改中”的等待提示
		if(data.result != '1'){
			msg.failure(data.info);
		}else{
			wm.token.set(data.info);	//更新token
			msg.success('操作成功', function(){
				//标记安装成功
				window.location.href="/install/setLocalDomain.do";
			});
		}
	});
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 