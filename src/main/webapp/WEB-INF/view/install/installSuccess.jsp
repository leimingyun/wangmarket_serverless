<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="安装成功"/>
</jsp:include>

  	<style>
  		
  		.content{
  			width: 600px;
  			min-height:80%;
		    margin: 0 auto;
		    box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
		    padding: 30px;
		    margin-top: 50px;
  		}
  		.title{
  			border-bottom: 1px solid #eee;
		    padding-top: 20px;
		    padding-left: 10px;
		    padding-bottom: 20px;
		    font-size: 28px;
		    margin-bottom: 20px;
		    text-align:center;
  		}
  		.content ul{
  			padding-left: 20px;
  		}
  		.content ul li{
  			list-style-type: decimal;
  			padding-left:10px;
  			padding-bottom:4px;
  		}
  		.content ul li img{
  			max-width:250px;
  			padding:4px;
  			padding-left:40px;
  		}
  		.info{
  			font-size:14px;
  			line-height: 22px;
  		}
  		.info h2,h3,h4,h5{
 			border-bottom: 1px solid #eee;
		    padding-top: 23px;
		    margin-bottom: 10px;
		    padding-bottom: 5px;
  		}
  		
  		@media only screen and (max-width: 700px) {
  			.content{
  				width:auto;
  				margin-top: 0px;
  				box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
  			}
  			
  		}
  		
  		a{
  			color: blue;
  		}
  	</style>
  	
    <div class="content">
    	<div class="title">
    		恭喜您，系统安装成功！
    		<br/>
    		<br/>
    		<br/>
    		<br/>
    	</div>
    	

    	<div class="info" style="float:left; width:350px;">
    		<h2>请牢记以下信息</h2>
    		登录地址: 
    		<script>
    			document.write('<a href="'+window.location.origin+'/login.do" target="_black">'+window.location.origin+'/login.do</a>');
			</script>
    		<br/>登陆账号：<b>wangzhan</b>
			<br/>登录密码：<b>wangzhan</b>
    	</div>
    	<div class="info" style="float:left;width:250px;">
    		<div style="width:100%; text-align:center;">
    			<img src="http://cdn.weiunity.com/site/1893/templateimage/dca2d002c6ca42da943d648cc62b8fdc.jpg" style="width:150px;" />
    		</div>
    		<div style="width: 100%;text-align: center;">
	    		<a href="http://www.leimingyun.com" target="_black" style="color: gray;font-size: 8px; padding-left:40px; padding-right:40px;">微信公众号：wangmarket</a>
	    	</div>
    	</div>
    	
    	
    </div>
    
  </body>
</html>