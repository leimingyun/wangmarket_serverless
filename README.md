## 该仓库已废弃。最新仓库：
https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms

#### 简介
网市场云建站系统-无服务器版，是一款深度结合华为云对象存储服务的CMS内容管理建站工具，您可以使用该工具对 OBS 进行常用的配置管理操作，如通过简单选择即可 自动创建指定属性的桶、自动为开通的桶配置好静态网站托管（如首页、404页面等）、绑定域名等。而您在这个过程中无需去增加任何学习，只需通过引导简单点击选择、下一步、选择、完成、即可完成整个步骤操作。  

#### 开源说明
本项目核心CMS管理系统的实现开源地址为 [https://gitee.com/mail_osc/wangmarket](https://gitee.com/mail_osc/wangmarket) 开源CMS建站中排名第二，拥有4k star关注。本项目是其在华为云OBS应用的衍生版本，将华为云的云存储-静态网站托管的优势最大化发挥出来，并能用于实际应用，让广大企业受益。

#### 功能

本工具提供建立网站所需要的模板管理、拿来即可导入使用的网站模板、后台内容管理系统等，您可以通过网站后台管理系统方便的进行更改网站页面、及内容管理、图片等文件上传操作，不需要懂代码也可以快速做个网站出来，使计算机专业刚毕业的大学生就能进行整个操作，从价格以及技术上全方位降低网站的门槛。

#### 适用范围

当前仅适用于 Windows 64位操作系统，包含

* Windows 7
* Windows 8.1
* Windows 10
* Windows 11

#### 工具优势

1. 简单、易用。按照引导步骤，下一步，下一步，完成，网站就出来了
2. 无需安装，即下即用
3. 模板可定制，网站想怎么显示，想怎么来，完全你说了算，无限扩展。

#### 使用场景

企业需要做一个自己的官网、个人做一个自己的简历/博客，不涉及到动态交互（如登录注册、下单购物等），只是单纯图文展示性质的网站，都可以使用本工具进行管理制作

#### 打包好的应用软件下载地址

http://down.zvo.cn/wangmarket/wangmarket_serverless_windows_x64.zip
当前只支持windows 64位系统使用。

#### 快速安装使用

###### 1. 解压出来

如下图，注意，不要放到中文路径的文件夹中

![image.png](https://images.gitee.com/uploads/images/2022/0518/175257_376a681a_429922.png)

###### 2. 启动

第一步：双击 [ 启动.bat ]   ,进行启动 （有的会隐藏后缀，看不到 .bat ，那直接双击 [启动] 即可）

![image.png](https://images.gitee.com/uploads/images/2022/0518/175822_efc3ceb3_429922.png)

###### 3. 启动成功

看到如下图所示，即表示软件已成功启动。

![image.png](https://images.gitee.com/uploads/images/2022/0518/180152_d254734d_429922.png)

###### 4. 使用

打开Chrome浏览器，在网址一栏，输入 localhost 并敲回车打开网址，即可按照提示引导，进行使用
（注意，建议用Chrome浏览器，如果实在没有可以用Edge、火狐、360极速模式也行）

![image.png](https://images.gitee.com/uploads/images/2022/0518/180830_0d913171_429922.png)

#### 更多

[![image.png](https://images.gitee.com/uploads/images/2022/0521/164224_8a6a1b75_429922.png)更多帮助，请查阅 http://doc.serverless.wangmarket.zvo.cn](http://doc.serverless.wangmarket.zvo.cn/)